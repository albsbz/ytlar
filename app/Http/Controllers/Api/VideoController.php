<?php

namespace App\Http\Controllers\Api;

use App\User;
use Alaouy\Youtube\Facades\Youtube;
use App\Http\Controllers\Api\PaginableController;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class VideoController extends PaginableController
{

    public function index(Request $request)
    {
        $channels=User::where('yt_channel', '!=', null)->get()->map(function ($user) {
            return $user['yt_channel'];
        })->toArray();
        $videos=array();
        foreach ($channels as $channelId) {
            try{
                $extrs= Youtube::listChannelVideos($channelId); 
                $videos=array_merge($videos, $extrs);
            } catch (\Exception $e) {}
        }
        return  parent::paginate($request, $videos); 
        return dd($videos);
    }
   
    public function show(Request $request, $channelId='')
    {
        try{
            if ($channelId) {
                $videos=Youtube::listChannelVideos($channelId, 10);
            } else{
                $videos=array();
            }
            return parent::paginate($request, $videos);
         } catch (\Exception $e) {}
    }
}
