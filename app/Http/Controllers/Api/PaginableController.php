<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;

class PaginableController extends Controller
{
    protected static function paginate($request, $videos){
        $perPage = $request->data['perpage'];
        $offset=$request->data['offset'];
        $page = $offset/$perPage;    
        $pagination['videos'] = array_slice($videos, $offset, $perPage);
        $pagination['pagecount']=ceil(count($videos)/$perPage);
        return  $pagination; 
    }
}
