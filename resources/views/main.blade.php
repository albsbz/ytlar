@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Main</div>
                <div class="card-body d-flex flex-column align-items-center mb-5" >
                    <div >All videos of registered users</div>
                   
             
                    <div id="root-ytlisting"  channelid="all">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
