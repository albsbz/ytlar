import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import Ytvideo from './Ytvideo';
import axios from 'axios';
import '../../sass/ytListing.scss';

import ReactPaginate from 'react-paginate';

function YtListing(props) {
    const [videos, setVideos] = useState([])
    const [offset, setOffset] = useState(0)
    const [pageCount, setPageCount] = useState(0)

    useEffect(()=>{
        loadVideosFromServer()
    }, [])

    useEffect(()=>{
        loadVideosFromServer()
    }, [offset])

    function loadVideosFromServer(){
        const requestUrl=(props.channelid==='all')?
            `${window.location.protocol}//${window.location.hostname}/api/allvideos`:
            `${window.location.protocol}//${window.location.hostname}/api/videos${props.channelid?'/'+props.channelid:''}`;
        axios.post(requestUrl, {
            data: { 
                perpage: props.perPage, 
                offset: offset
            }
        })
        .then(res => {
            setVideos(res.data.videos)
            setPageCount(res.data.pagecount)
        }).catch(error => {
            console.log("error", error);
        }); 
    }

    const handlePageClick = data => {
        let selected = data.selected;
        let offset = Math.ceil(selected * props.perPage);
        setOffset(offset)
    };
    
    let list
    if (props.channelid===''){
        list=<div>User has no channel</div>
    }else if(!videos){
        list=<div>User has no videos</div>
    } else{
        list=Array.from(videos).filter(video=>video).map(video=>
            <div className="yt-video" key={video.id.videoId}>
                <Ytvideo videoid={video.id.videoId} />
            </div>
        )     
    }
    return (
        <div className="d-flex flex-column justify-content-center align-items-center">
            {list}
            {  
                (videos&&props.channelid)?
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination '}
                    activeClassName={'active'}
                    pageClassName={'page-item'}
                    pageLinkClassName={'page-link'}
                    previousLinkClassName={'page-link'}
                    nextLinkClassName={'page-link'}
                />
                :''
            }
        </div>
    )        
   
}
    
const container=document.getElementById('root-ytlisting')
ReactDOM.render(
    <YtListing
        channelid={container.getAttribute('channelid')}
        perPage={3}
    />,
    container
);
