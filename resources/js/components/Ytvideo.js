import React from 'react';
import YouTube from '@u-wave/react-youtube';

function Ytvideo (props) {
  return (
    <YouTube
      video={props.videoid}
    />
  )

}
export default Ytvideo;

// ReactDOM.render(<Ytvideo />, document.getElementById('example'));

